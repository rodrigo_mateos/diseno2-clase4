﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ecommerce.Entities
{
   public class OrderLine
    {
       public int OrderLineId { get; set; }
       public double Amount { get; set; }
       public Product Product { get; set; }

       public OrderLine()
       {
           
       }
    }
}
